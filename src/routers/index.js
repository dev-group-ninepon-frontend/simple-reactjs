import {
    BrowserRouter,
    Routes,
    Route,
    Router,
} from "react-router-dom";

//pages
import LayoutNavbar from "layouts/LayoutNavbar"
import PageHomeIndex from "pages/Home/index"
import PageAboutMeIndex from "pages/AboutMe/index"
import PageNotificationIndex from "pages/Notification/index"
// import PageCartIndex from "pages/Cart/index"
import PageError404 from "pages/Error/404"

//redux
import { Provider } from 'react-redux';
import { storeGeneral } from 'redux/stores/storeGeneral'
// import { storeCart } from 'redux/stores/storeCart'

const CreateRoutes = () => (
  <>
    <Provider store={storeGeneral}>
      <Routes>
        <Route path="/" element={<LayoutNavbar />}>
          <Route index element={<PageHomeIndex />} />
          <Route path="home" element={<PageHomeIndex />} />
          <Route path="profile" element={<PageAboutMeIndex />} />
          <Route path="about" element={<PageAboutMeIndex />} />
          <Route path="notification" element={<PageNotificationIndex />} />
          <Route path="*" element={<PageError404 />} />
        </Route>
      </Routes>
    </Provider>
    {/* <Provider store={storeCart}>
      <Routes>
        <Route path="/about" element={<LayoutNavbar />}>
          <Route path="about" element={<PageAboutMeIndex />} />
        </Route>
      </Routes>
    </Provider> */}
  </>
);

export default CreateRoutes;