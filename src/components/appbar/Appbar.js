import * as React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import AdbIcon from '@mui/icons-material/Adb';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Drawer from '@mui/material/Drawer';
import { Link } from "react-router-dom";
import NotificationsIcon from '@mui/icons-material/Notifications';
import Badge from '@mui/material/Badge';
import Popover from '@mui/material/Popover';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import MenuList from '@mui/material/MenuList';
import ListItemIcon from '@mui/material/ListItemIcon';

//fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid, regular } from '@fortawesome/fontawesome-svg-core/import.macro'

//config
import ConfigData from "configs/appConfig.json";
import ProfileMenu from "configs/profileMenu";
import navItems from "configs/drawerMenu";

//components
import BasicSwitch from 'components/switch/BasicSwitch'

//redux
import { useDispatch, useSelector } from 'react-redux';

//firebase
import { collection, addDoc, getDocs, onSnapshot, setDoc, doc, getFirestore } from 'firebase/firestore'
import { db } from 'services/firebase' 

const pages = ['Products', 'Pricing', 'Blog'];
const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];
const drawerWidth = 240;

export default function ButtonAppBar(props) {
  const { window } = props;
  const dispatch = useDispatch();
  const container = window !== undefined ? () => window().document.body : undefined;
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [dektopOpen, setDesktopOpen] = React.useState(false);
  const [numberUnreadNotify, setNumberUnreadNotify] = React.useState(0);

  //notification
  const [popUpNotification, setPopUpNotification] = React.useState(false);
  const [arrayLast10Notification, setLast10Notification] = React.useState([]);
  const [tabNotificationContents, setTabNotificationContents] = React.useState('all');

  const [darkTheme, setDarkTheme] = React.useState(false);

  React.useEffect(() => {
    const collectionPath = collection(db, 'users','reactjs','notifications');
    onSnapshot(collectionPath, (snapshot) => {
        let unRead = 0
        setLast10Notification([])
        const updatedNotifications = snapshot.docs.map((doc) => doc.data());
  
        updatedNotifications.map((item)=>{
          if(!item.is_read){
            unRead++
            if(unRead < 10){
              setLast10Notification( oldArray => [...oldArray, item] )
            }
          }
        })
        setNumberUnreadNotify(unRead)
    });
  },[]);

  const handleDarkTheme =()=>{
    setDarkTheme(!darkTheme);
  };


  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleTabNotificationContents = (event, newValue) => {
    setTabNotificationContents(newValue);
  };


  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = (menu) => {
    console.log(menu)
    setAnchorElUser(null);
  };

  const handleOpenNotification = (menu) => {
    setPopUpNotification(true);
  };

  const handleCloseNotification = (menu) => {
    setPopUpNotification(false);
  };

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
    setDesktopOpen(!dektopOpen)
  };

  const mobileDrawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
      <Typography variant="h6" sx={{ my: 2 }}>
        MUI
      </Typography>
      <Divider />
      <List>
        {navItems.map((item) => (
          <ListItem key={item.route} disablePadding button component={Link} to={item.route}>
            <ListItemButton sx={{ textAlign: 'center' }}>
              <ListItemText primary={item.title}  />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>

          {/* DRAWER MENU */}
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            onClick={handleDrawerToggle}
          >
            <MenuIcon />
          </IconButton>

          {/* APP NAME */}
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            {ConfigData.APP_NAME}
          </Typography>

          <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
            {navItems.map((item) => (
              <Button key={item.route} sx={{ color: '#fff' }}>
                {/* <Link to={item.route}>{item.title}</Link> */}
              </Button>
            ))}
          </Box>

          {/* NOTOFICATION */}
          <Box sx={{ flexGrow: 0 }}>
            <IconButton
              size="large"
              aria-label="show new notifications"
              color="inherit"
              onClick={handleOpenNotification}
              id="appbar-notify-button"
            >
              <Badge badgeContent={numberUnreadNotify} color="error">
                <FontAwesomeIcon icon={solid("bell")} />
              </Badge>
            </IconButton>
            <Popover
              id="menu-appbar"
              anchorReference="appbar-notify-button"
              open={Boolean(popUpNotification)}
              anchorEl={popUpNotification}
              onClose={handleCloseNotification}
              anchorOrigin={{
                vertical: 'center',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              keepMounted
            >
              <Button 
                variant="contained" 
                startIcon={<FontAwesomeIcon icon={solid("check-double")} />} 
                style={{marginTop: '1em', marginLeft: '1em', right: '0px'}}
                onClick={ async ()=> await markReadNotifyAll() }
              >
                mark read all
              </Button>
              <TabContext value={tabNotificationContents}>
                <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                  <TabList onChange={handleTabNotificationContents} aria-label="lab API tabs example">
                    <Tab label="ALL" value="all" />
                    <Tab label="INFO" value="info" />
                    <Tab label="ERROR" value="error" />
                  </TabList>
                </Box>
                <TabPanel value="all">
                  <MenuList>
                    { arrayLast10Notification.map((item) => {
                      return (
                        <MenuItem>
                          <ListItemIcon>
                            <FontAwesomeIcon icon={solid("list")} />
                          </ListItemIcon>
                          <ListItemText>{item.title}</ListItemText>
                        </MenuItem>
                      )
                    }) }
                    {
                      numberUnreadNotify > 10 
                        ? 
                          <>
                          <Divider />
                          <MenuItem>
                            <ListItemIcon>
                              <FontAwesomeIcon icon={solid("maximize")} />
                            </ListItemIcon>
                            <ListItemText>
                              show more...
                            </ListItemText>
                          </MenuItem>
                          </>
                        : <></>
                    }
                  </MenuList>
                </TabPanel>
                <TabPanel value="info">
                  <MenuList>
                    { arrayLast10Notification.map((item) => {
                      if(item.level === 'info'){
                        return (
                          <MenuItem>
                            <ListItemIcon>
                              <FontAwesomeIcon icon={solid("circle-info")} />
                            </ListItemIcon>
                            <ListItemText>{item.title}</ListItemText>
                          </MenuItem>
                        )
                      }
                    }) }
                  </MenuList>
                </TabPanel>
                <TabPanel value="error">
                  <MenuList>
                    { arrayLast10Notification.map((item) => {
                      if(item.level === 'error'){
                        return (
                          <MenuItem>
                            <ListItemIcon>
                              <FontAwesomeIcon icon={solid("circle-xmark")} />
                            </ListItemIcon>
                            <ListItemText>{item.title}</ListItemText>
                          </MenuItem>
                        )
                      }
                    }) }
                  </MenuList>
                </TabPanel>
              </TabContext>
            </Popover>
          </Box>

          {/* PROFILE */}
          <Box sx={{ flexGrow: 0 }}>
            <Tooltip title="Open settings">
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                <Avatar alt="Remy Sharp" src="/static/images/avatar/2.jpg" />
              </IconButton>
            </Tooltip>
            <Menu
              sx={{ mt: '45px' }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              { ProfileMenu.map((item) => {
                return (
                  <MenuItem key={item.route} onClick={handleCloseUserMenu}>
                    {item.icon}
                    <Link to={item.route}>{item.title}</Link>
                    <Typography textAlign="center" style={{paddingLeft: '8px'}}>{item.title}</Typography>
                  </MenuItem>
                )
              }) }
            </Menu>
          </Box>

        </Toolbar>
      </AppBar>

      <Box component="nav">
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
          {mobileDrawer}
        </Drawer>

        <Drawer
          container={container}
          variant="temporary"
          open={dektopOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'none', sm: 'block' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}
        >
          {mobileDrawer}
        </Drawer>
      </Box>
    </Box>
  );
}

async function markReadNotifyAll(){
  const notificationsRef = collection(db, 'users','reactjs','notifications'); 
  const alldocs = await getDocs(notificationsRef);

  const docs = alldocs.docs.map((doc) => doc.id);
  await docs.map( async (item) => {
    const docRef = doc(db, 'users','reactjs','notifications', item);
    await setDoc(docRef, { is_read: true }, { merge: true });
  })
}