import React , { Component , useState, useEffect } from 'react';
import { Routes, Route, Outlet, Link } from "react-router-dom";
import ButtonAppBar from "components/appbar/Appbar"

//theme
import { ThemeProvider, makeStyles } from '@mui/styles';

//font
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid, regular } from '@fortawesome/fontawesome-svg-core/import.macro'

const themeInstance = {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
};

const useStyles = makeStyles((theme) => ({
    root: {
        background: theme.background,
        border: 0,
        fontSize: 16,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
    },
}));

function DeepChild() {
    const classes = useStyles();
  
    return (
      <button type="button" className={classes.root}>
        Theming
      </button>
    );
  }
  

const LayoutNavbar =()=>{
    return(
        <>
            <ButtonAppBar/>
            <div style={{paddingLeft: '5px', paddingRight: '5px'}}>
                {/* <FontAwesomeIcon icon={solid('bolt-lightning')}  /> */}
                <p>fixed component</p>
                {/* <ThemeProvider theme={themeInstance}>
                    <DeepChild />
                </ThemeProvider> */}
                <Outlet />
            </div>
        </>
    )
}

export default LayoutNavbar