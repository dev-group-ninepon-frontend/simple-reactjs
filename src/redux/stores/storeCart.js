import { configureStore } from '@reduxjs/toolkit';
import cartReducer from 'redux/features/shop/cartSlice'

export const storeCart = configureStore({
    reducer: {
        cart: cartReducer,
    }
})