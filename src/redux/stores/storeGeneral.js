import { configureStore } from '@reduxjs/toolkit';
import counterReducer from 'redux/features/general/counterSlice'
import helperReducer from 'redux/features/general/helperSlice'

export const storeGeneral = configureStore({
    reducer: {
        counter: counterReducer,
        helper: helperReducer,
    }
})