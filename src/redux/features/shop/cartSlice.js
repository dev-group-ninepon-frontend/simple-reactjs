import { createAsyncThunk, createSlice, nanoid } from "@reduxjs/toolkit";

const initialState = {
    price: 0,
    listItem: []
}

export const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        addToCart: (state, action) =>{
            let duplicate = state.listItem.filter(item=> 
                item.code === action.payload.code
            )
            if(duplicate.length <= 0){
                state.listItem.push( action.payload.item )
                state.price += action.payload.price
            }
        },
        reCaluatePrice: (state, action) =>{
            state.listItem.map((item)=>{
                state.price += item.price
            })
        },
    },
})

export const { addToCart, reCaluatePrice } = cartSlice.actions
export default cartSlice.reducer