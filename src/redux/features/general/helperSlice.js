import { createAsyncThunk, createSlice, nanoid } from "@reduxjs/toolkit";

const initialState = {
    stringValue1: "",
    stringValue2: ""
}

export const helperSlice = createSlice({
    name: 'helper',
    initialState,
    reducers: {
        randomNumber: (state, action) =>{
            state.stringValue1 = "0000"
            state.stringValue2 = "1111"
        },
        randomString: (state, action) =>{
            state.stringValue1 = "Aaaa"
            state.stringValue2 = "Bbbb"
        },
    },
})

export const { randomNumber, randomString } = helperSlice.actions
export default helperSlice.reducer