import { createAsyncThunk, createSlice, createAction, nanoid } from "@reduxjs/toolkit";

const initialState = {
    value: 0,
    value2: 0
}

//this reducer work about "counter" as same name itself
//export (object) createSlice return actions and reducer
export const counterSlice = createSlice({
    name: 'counter',
    initialState,
    reducers: {
        resetCountToInit: (state, action)=> {
            state.value = 0
        },
        increment: (state, action) =>{
            // state is value for action and return back to reducer
            // action is payload, can send anything
            state.value += 1
        },
        decrement:{
            // we can separated reducer kind of this
            reducer: (state, action) => {
                state.value -= 1
            },
        },
        incrementByAmount: {
            reducer: (state, action) => {
                // console.log(`incrementByAmount reducer`)
                // console.log(action)
                state.value += action.payload.number
            },
            // if use prepare, reducer these not affect return anything because prepare will return value instead
            // if use const res = { payload: { id: id, payload } } for condition
            // example prepare return: payload: Object { id: "HSrSukv_QhOjAsxJpr4hi", payload: { number: 8 } }
            prepare: (payload) => {
                // console.log(payload)
                return { payload }
            },
        },
        decrementByAmount: {
            reducer: (state, action) => {
                state.value -= action.payload.number
            },
            prepare: (payload) => {
                return { payload }
            },
        },
    },
})

//export (object) all action for dispatch
export const { resetCountToInit, increment, decrement, incrementByAmount } = counterSlice.actions

//export state name and value to store
export default counterSlice.reducer