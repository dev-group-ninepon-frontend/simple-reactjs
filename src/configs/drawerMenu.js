import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid, regular } from '@fortawesome/fontawesome-svg-core/import.macro'

export default [
    {
        "title": "home",
        "route": "home",
        "icon": <FontAwesomeIcon icon={solid('address-card')} />
    },
    {
        "title": "profile",
        "route": "profile",
        "icon": <FontAwesomeIcon icon={solid('address-card')} />
    },
    {
        "title": "about me",
        "route": "about",
        "icon": <FontAwesomeIcon icon={solid('address-card')} />
    },
    {
        "title": "notification system",
        "route": "notification",
        "icon": <FontAwesomeIcon icon={solid('address-card')} />
    }
]