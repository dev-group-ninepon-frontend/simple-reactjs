import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid, regular } from '@fortawesome/fontawesome-svg-core/import.macro'

export default [
    {
        "title": "profile",
        "route": "profile",
        "icon": <FontAwesomeIcon icon={solid('address-card')} />
    },
    {
        "title": "account",
        "route": "account",
        "icon": <FontAwesomeIcon icon={solid('user')} />
    },
    {
        "title": "dashboard",
        "route": "account",
        "icon": <FontAwesomeIcon icon={solid('gauge')} />
    },
    {
        "title": "logout",
        "route": "logout",
        "icon": <FontAwesomeIcon icon={solid('right-from-bracket')} />
    }
]