import React from 'react';
import { green, pink } from "@mui/material/colors";
import { createTheme } from "@mui/material/styles";

//NOTE default theme https://mui.com/material-ui/customization/default-theme/#main-content
export default createTheme({
  palette: {
    // primary: purple,
    // secondary: green,
  },
  status: {
    danger: 'orange',
  },
});