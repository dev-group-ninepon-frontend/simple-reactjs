import React, { useState } from 'react';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import { useDispatch, useSelector } from 'react-redux';
import randomstring from "randomstring";
import randomWords  from "random-words";


import { collection, addDoc, getDocs, deleteDoc, doc, FieldValue, onSnapshot, writeBatch } from 'firebase/firestore'
import { db } from 'services/firebase' 

//call action 
import { getNotificationList } from 'redux/features/notifications/notificationSlice';

const PageNotificationIndex = ()=>{
    //get state name counter >> count
    const dispatch = useDispatch()
    const count = useSelector((state)=> { 
        // console.log(state.counter) ; 
        // dispatch( getNotificationList() )
        return state.counter.value  
    })

    return(
        <>
            <Button
                variant="contained" 
                onClick={ async ()=> await createNotify() }
            >
                Add new set notification
            </Button>
            
            <Button
                variant="contained" 
                onClick={ async ()=> await deleteNotify() }
                style={{ marginTop: '1em' }}
            >
                delete all notification
            </Button>
        </>
    )
}

async function createNotify(){
    const notificationsRef = collection(db, 'users','reactjs','notifications'); 

    const set10NewNotifications = 
    [{
        is_read: false,
        level: 'info',
        title: randomWords({ exactly: 1, join: ' ' }),
        message: randomWords({ exactly: 10, join: ' ' }),
        timestamp: new Date()
    },{
        is_read: false,
        level: 'info',
        title: randomWords({ exactly: 1, join: ' ' }),
        message: randomWords({ exactly: 10, join: ' ' }),
        timestamp: new Date()
    },{
        is_read: false,
        level: 'error',
        title: randomWords({ exactly: 1, join: ' ' }),
        message: randomWords({ exactly: 10, join: ' ' }),
        timestamp: new Date()
    },{
        is_read: false,
        level: 'error',
        title: randomWords({ exactly: 1, join: ' ' }),
        message: randomWords({ exactly: 10, join: ' ' }),
        timestamp: new Date()
    },{
        is_read: false,
        level: 'info',
        title: randomWords({ exactly: 1, join: ' ' }),
        message: randomWords({ exactly: 10, join: ' ' }),
        timestamp: new Date()
    }]

    set10NewNotifications.forEach(async (obj) => {
        await addDoc(notificationsRef, obj)
    })
}

async function deleteNotify(){
    const notificationsRef = collection(db, 'users','reactjs','notifications'); 
    const alldocs = await getDocs(notificationsRef);
    const docs = alldocs.docs.map((doc) => doc.id);
    await docs.map( async (item) => {
        await deleteDoc( doc(db, 'users','reactjs','notifications', item) );
    })
}

export default PageNotificationIndex;