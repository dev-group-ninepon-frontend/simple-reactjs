import React, { useState } from 'react';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import { useDispatch, useSelector } from 'react-redux';

//call action 
import { resetCountToInit, increment, decrement, incrementByAmount } from 'redux/features/general/counterSlice';

import ButtonAppBar from "components/appbar/Appbar"

const PageHomeIndex = ()=>{
    //get state name counter >> count
    const dispatch = useDispatch()
    const count = useSelector((state)=> { 
        // console.log(state.counter) ; 
        if(isNaN(state.counter.value)){ 
            // console.log(`below is NaN`)
            // console.log(state)
            dispatch( resetCountToInit())
        }
        return state.counter.value  
    })

    return(
        <>
            <Stack spacing={2} direction="row">
                <h1>{ count }</h1>
                <Button 
                    variant="contained"
                    onClick={()=> dispatch( increment({
                        number: 9
                    }) )}
                >+</Button>
                <Button 
                    variant="outlined"
                    onClick={()=> dispatch( decrement({
                        number: 8
                    }) )}
                >-</Button>
            </Stack>
            <Stack spacing={2} direction="row">
            <Button 
                variant="outlined"
                onClick={()=> dispatch( incrementByAmount({
                    number: 8
                }) )}
            >plus with callback</Button>
            </Stack>
        </>
    )
}

export default PageHomeIndex;