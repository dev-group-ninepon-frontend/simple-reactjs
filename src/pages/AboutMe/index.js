import React, { useState } from 'react';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { useDispatch, useSelector } from 'react-redux';

//call action 
import { increment, decrement, incrementByAmount } from 'redux/features/general/counterSlice';

// import ButtonAppBar from "components/appbar/Appbar"

const PageAboutMeIndex = ()=>{
    const count = useSelector((state)=> state.counter.value)
    const dispatch = useDispatch()

    return(
        <>
            <Grid container direction="row" spacing={2}>
                <Grid item xs container direction="column">
                    <center>
                        <h3>ABOUT ME</h3>
                    </center>
                </Grid>
                
            </Grid>
        </>
    )
}

export default PageAboutMeIndex;